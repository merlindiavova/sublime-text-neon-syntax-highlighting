# Neon Syntax Highlighting
Neon syntax highlighting for Sublime Text.

## Installation
### Via [Package Control](https://packagecontrol.io/installation):
1. Press `Control + Shift + P` on Windows/Linux or `Command + Shift + P` on OS X
2. Search `NEON Syntax Highlighting`
3. Press `Enter`
4. **Complete!**

### Via [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git):
1. Go to your Sublime Text packages folder (In ST3 `Preferences -> Browse Packages...`)
2. `git clone http://gitlab.com/merlindiavova/sublime-text-neon-syntax-highlighting.git`
3. **Complete!**
